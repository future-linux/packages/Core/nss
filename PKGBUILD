# Maintainer: Future Linux Team <futurelinux@163.com>

pkgname=nss
pkgver=3.99
pkgrel=1
pkgdesc="Network Security Services"
arch=('x86_64')
url="https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS"
license=('GPL')
depends=('nspr>=4.35' 'p11-kit>=0.23.19' 'sqlite' 'zlib' 'bash')
makedepends=('perl' 'python')
source=(https://archive.mozilla.org/pub/security/nss/releases/NSS_${pkgver//./_}_RTM/src/${pkgname}-${pkgver}.tar.gz
	${pkgname}-${pkgver}-standalone-1.patch)
sha256sums=(5cd5c2c8406a376686e6fa4b9c2de38aa280bea07bf927c0d521ba07c88b09bd
	87bb1af0b11fd41311b9899187f6e4b3fca9940651123c7bc836ec7497d2da84)

prepare() {
	cd ${pkgname}-${pkgver}

	patch -Np1 -i ${srcdir}/${pkgname}-${pkgver}-standalone-1.patch
}

build() {
	cd ${pkgname}-${pkgver}/nss

	make BUILD_OPT=1                            \
		NSPR_INCLUDE_DIR=/usr/include/nspr  \
		NSPR_LIB_DIR=/usr/lib64             \
		USE_SYSTEM_ZLIB=1                   \
		ZLIB_LIBS=-lz                       \
		NSS_ENABLE_WERROR=0                 \
		USE_64=1                            \
		NSS_USE_SYSTEM_SQLITE=1

}

package() {
	cd ${pkgname}-${pkgver}/dist

	install -vdm755 ${pkgdir}/usr/{bin,include/nss,lib64/pkgconfig}

	install -v -m755 Linux*/lib/*.so              ${pkgdir}/usr/lib64
	install -v -m644 Linux*/lib/{*.chk,libcrmf.a} ${pkgdir}/usr/lib64

	cp -v -RL {public,private}/nss/*              ${pkgdir}/usr/include/nss

	install -v -m755 Linux*/bin/{certutil,nss-config,pk12util} ${pkgdir}/usr/bin

	install -v -m644 Linux*/lib/pkgconfig/nss.pc  ${pkgdir}/usr/lib64/pkgconfig
}
